IZG Cvičení 4
-------------
Body: 3/3

#### Ovládání aplikace
##### Při spuštění je předán jako první parametr soubor obsahující model
Vykreslení příslušného modelu jako hlavního objektu (implicitně cylinder.tri)
Na výběr: cylinder.tri, sphere.tri, bunny.tri či vlastní model

##### Stisknutí levého tlačítka myši a současné tažení kurzorem
Rotace celé scény kolem jejího středu (středu zrcadla)

##### Stisknutí pravého tlačítka myši a současné tažení kurzorem
Přibližování či oddalování středu scény (středu zrcadla) od pozorovatele

##### Stisknutí levého tlačítka myši, stisknutí klávesy CTRL a současné tažení kurzorem
Rotace objektu kolem svého středu

##### Stisknutí levého tlačítka myši, stisknutí klávesy SHIFT a současné tažení kurzorem
Posun středu objektu (a tedy i objektu samotného) ve smyslu původních os X resp. Y (směrem od a k zrcadlu resp. podél zrcadla nahoru a dolů)

##### Stisknutí klávesy Q, X nebo ESC
Ukončení běžící aplikace